Source: lxd
Section: admin
Priority: optional
Maintainer: Michael Jeanson <mjeanson@debian.org>
Standards-Version: 4.4.1
Homepage: https://linuxcontainers.org/
Vcs-Git: https://salsa.debian.org/mjeanson/lxd.git
Vcs-Browser: https://salsa.debian.org/mjeanson/lxd
Build-Depends: debhelper (>= 10),
               dh-apparmor,
               dh-golang,
               golang-go,
               help2man,
               libacl1-dev,
               libapparmor-dev,
               libcap-dev,
               libco-dev,
               libdqlite-dev,
               liblxc-dev (>= 1.1.0~),
               libraft-dev,
               libseccomp-dev,
               libsqlite3-dev,
               libudev-dev,
               libuv1-dev,
               pkg-config,
               python3-lxc,
               tcl8.6-dev
XS-Go-Import-Path: github.com/lxc/lxd

Package: lxd-client
Architecture: amd64 arm64 armhf i386 ppc64el s390x
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Suggests: lxd
Breaks: lxc1 (<= 2.0.0~rc13-0ubuntu2~)
Replaces: lxc1 (<= 2.0.0~rc13-0ubuntu2~)
Description: Container hypervisor based on LXC - client
 LXD offers a REST API to remotely manage containers over the network,
 using an image based workflow and with support for live migration.
 .
 This package contains the command line client.

Package: lxd
Architecture: amd64 arm64 armhf i386 ppc64el s390x
Depends: acl,
         adduser,
         dnsmasq-base,
         ebtables,
         iproute2,
         iptables,
         liblxc1 (>= 2.1.0~),
         lsb-base (>= 3.0-6),
         lxcfs,
         lxd-client (= ${binary:Version}),
         passwd (>= 1:4.1.5.1-1ubuntu5~),
         rsync,
         squashfs-tools,
         uidmap (>= 1:4.1.5.1-1ubuntu5~),
         xdelta3,
         xz-utils,
         ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Recommends: apparmor
Suggests: criu, lxd-tools
Description: Container hypervisor based on LXC - daemon
 LXD offers a REST API to remotely manage containers over the network,
 using an image based workflow and with support for live migration.
 .
 This package contains the LXD daemon.

Package: lxd-tools
Architecture: amd64 arm64 armhf i386 ppc64el s390x
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Container hypervisor based on LXC - extra tools
 LXD offers a REST API to remotely manage containers over the network,
 using an image based workflow and with support for live migration.
 .
 This package contains extra tools provided with LXD.
  - fuidshift - A tool to map/unmap filesystem uids/gids
  - lxc-to-lxd - A tool to migrate LXC containers to LXD
  - lxd-benchmark - A benchmarking tool for LXD
